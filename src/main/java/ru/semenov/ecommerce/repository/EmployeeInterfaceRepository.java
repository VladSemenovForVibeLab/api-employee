package ru.semenov.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.ecommerce.entity.Employee;

@Repository
@RepositoryRestResource
public interface EmployeeInterfaceRepository extends JpaRepository<Employee, String> {
}
