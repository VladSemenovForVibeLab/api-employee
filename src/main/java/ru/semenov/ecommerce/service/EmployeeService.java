package ru.semenov.ecommerce.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.semenov.ecommerce.entity.Employee;
import ru.semenov.ecommerce.model.EmployeeModel;
import ru.semenov.ecommerce.repository.EmployeeInterfaceRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeInterfaceRepository employeeInterfaceRepository;
    public ResponseEntity<Employee> addEmployee(EmployeeModel employee) {
        Employee employee1 = Employee.builder().employeeName(employee.getEmployeeName()).email(employee.getEmail()).build();
        return ResponseEntity.ok(employeeInterfaceRepository.save(employee1));
    }

    public ResponseEntity<Employee> getEmployee(String id) {
        Optional<Employee> optionalEmployee = employeeInterfaceRepository.findById(id);
        if (!optionalEmployee.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(optionalEmployee.get());
    }

    public ResponseEntity<List<Employee>> getEmployee() {
        return ResponseEntity.ok(employeeInterfaceRepository.findAll());
    }

    public ResponseEntity<Employee> updateEmployee(String id, EmployeeModel employee) {
        Optional<Employee> optionalEmployee = employeeInterfaceRepository.findById(id);
        if (!optionalEmployee.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Employee employee1 = optionalEmployee.get();
        employee1.setEmployeeName(employee.getEmployeeName());
        employee1.setEmail(employee.getEmail());
        employeeInterfaceRepository.save(employee1);
        return ResponseEntity.ok(employee1);
    }

    public ResponseEntity<?> deleteEmployee(String id) {
        Optional<Employee> optionalEmployee = employeeInterfaceRepository.findById(id);
        if (!optionalEmployee.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        employeeInterfaceRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
