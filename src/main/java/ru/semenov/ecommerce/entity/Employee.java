package ru.semenov.ecommerce.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = Employee.EMPLOYEE_TABLE_NAME)
public class Employee {
    public static final String EMPLOYEE_TABLE_NAME = "employees";
    private static final String EMPLOYEE_ID = "id";
    private static final String EMPLOYEE_NAME = "name";
    private static final String EMPLOYEE_EMAIL = "email";
    @Id
    @UuidGenerator
    @Column(name = EMPLOYEE_ID, unique = true, updatable = false)
    private String id;
    @Column(name = EMPLOYEE_NAME,nullable = false)
    private String employeeName;
    @Column(name = EMPLOYEE_EMAIL,nullable = false)
    private String email;
}
