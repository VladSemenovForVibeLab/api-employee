package ru.semenov.ecommerce.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class EmployeeModel {
    private String employeeName;
    private String email;
}
