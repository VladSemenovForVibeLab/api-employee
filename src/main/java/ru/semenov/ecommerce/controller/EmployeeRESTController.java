package ru.semenov.ecommerce.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.ecommerce.entity.Employee;
import ru.semenov.ecommerce.model.EmployeeModel;
import ru.semenov.ecommerce.service.EmployeeService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(EmployeeRESTController.EMPLOYEE_URL)
public class EmployeeRESTController {
    private final EmployeeService employeeService;
    public static final String EMPLOYEE_URL = "/api/v1/employee";
   @PostMapping(path = "/add")
    public ResponseEntity<Employee> addEmployee(@RequestBody EmployeeModel employee) {
       return employeeService.addEmployee(employee);
   }
   @GetMapping(path = "/get/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable String id) {
       return employeeService.getEmployee(id);
   }
   @GetMapping(path = "/get")
    public ResponseEntity<List<Employee>> getEmployee() {
       return employeeService.getEmployee();
   }
   @PutMapping(path = "/update/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") String id, @RequestBody EmployeeModel employee) {
       return employeeService.updateEmployee(id, employee);
   }
   @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") String id) {
       return employeeService.deleteEmployee(id);
   }
}
